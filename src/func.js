let getSum = (str1, str2) => {
  if(!Number(str1) && !Number(str2))
   {
     return false;
   }
   return String(Number(str1)+Number(str2));
  };
 const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
   const filteredPost = listOfPosts.filter(function(item) {
     if(item['author'] === authorName) {
       return true;
     }
   })
 const postCount = filteredPost.length
 let counter = 0
 listOfPosts.forEach(function(item) {
   const comments = item['comments']
   if (comments !== undefined){
     comments.forEach(function(item2) {
        if(item2['author'] === authorName) {
           counter++
         } 
       })
   }
 })
 return `Post:${postCount},comments:${counter}`
  };
 const tickets=(people)=> {
   let total = {
     25: 0,
     50: 0
   }
   for (let i = 0; i < people.length; i++) {
     const bill = parseInt(people[i])
     
     if (bill === 25) {
       total['25'] +=1
       continue
     }
     let rest = bill - 25
     if(rest === 75) {
       if(total['50'] && total['25']) {
         total['50']--
         total['25']--
         continue
       }
       if(total['25'] >=3) {
         total['25'] -=3
         continue
       }
       return 'NO'
     }
     if(rest === 25) {
       if(total['25']) {
         total['25']--
         total['50']++
         continue;
       }
       return 'NO'
     }
   }
   return 'YES';
 };
 module.exports = {getSum, getQuantityPostsByAuthor, tickets};
